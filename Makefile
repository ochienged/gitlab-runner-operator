# Current Operator version
VERSION ?= v0.0.1
# Even if an empty VERSION is passed we override it
# since we always need VERSION. This is useful for dev builds
# of the operator and bundle images
ifeq ($(VERSION),)
override VERSION := v0.0.1
endif

# The Revision is used for unofficial builds since we need a semver-compatible
# version for the bundle
ifneq ($(REVISION),)
override VERSION := $(VERSION)-$(REVISION)
endif

# Current GitLab Runner version
APP_VERSION ?= v$(shell cat APP_VERSION)

MOCKERY_VERSION ?= 1.1.0
MOCKERY ?= .tmp/mockery-$(MOCKERY_VERSION)

# Toggles between building certified images for RedHat's registry
# or building for the GitLab registry
CERTIFIED ?= true

GITLAB_RUNNER_OPERATOR_REGISTRY ?= registry.gitlab.com/gitlab-org/gl-openshift/gitlab-runner-operator

# Default bundle image tag
BUNDLE_IMG ?= $(GITLAB_RUNNER_OPERATOR_REGISTRY)/gitlab-runner-operator-bundle:$(VERSION)
GITLAB_CHANGELOG_VERSION ?= master
GITLAB_CHANGELOG = .tmp/gitlab-changelog-$(GITLAB_CHANGELOG_VERSION)
# Options for 'bundle-build'
BUNDLE_CHANNELS ?= stable
BUNDLE_DEFAULT_CHANNEL ?= stable
BUNDLE_METADATA_OPTS ?= --channels=$(BUNDLE_CHANNELS) --default-channel=$(BUNDLE_DEFAULT_CHANNEL)

# Image URL to use all building/pushing image targets
IMG ?= $(GITLAB_RUNNER_OPERATOR_REGISTRY)/gitlab-runner-operator:$(VERSION)
# Produce CRDs that work back to Kubernetes 1.11 (no version conversion)
CRD_OPTIONS ?= "crd:trivialVersions=true,preserveUnknownFields=false"

SHELL=/bin/bash

PLATFORMS ?= linux/amd64
PLATFORM ?= linux/amd64
ARCH ?= amd64
OS ?= linux

# Get the currently used golang install path (in GOPATH/bin, unless GOBIN is set)
ifeq (,$(shell go env GOBIN))
GOBIN=$(shell go env GOPATH)/bin
else
GOBIN=$(shell go env GOBIN)
endif

all: manager

# Run tests
ENVTEST_ASSETS_DIR = $(shell pwd)/testbin
test: generate fmt vet manifests
	mkdir -p $(ENVTEST_ASSETS_DIR)
	test -f $(ENVTEST_ASSETS_DIR)/setup-envtest.sh || curl -sSLo $(ENVTEST_ASSETS_DIR)/setup-envtest.sh https://raw.githubusercontent.com/kubernetes-sigs/controller-runtime/v0.6.3/hack/setup-envtest.sh
	source $(ENVTEST_ASSETS_DIR)/setup-envtest.sh; fetch_envtest_tools $(ENVTEST_ASSETS_DIR); setup_envtest_env $(ENVTEST_ASSETS_DIR); go test ./... -coverprofile cover.out

# Build manager binary
manager: generate fmt vet
	go build -o bin/manager main.go

# Run against the configured Kubernetes cluster in ~/.kube/config
run: generate fmt vet manifests
	go run ./main.go

# Install CRDs into a cluster
install: manifests kustomize
	$(KUSTOMIZE) build config/crd | kubectl apply -f -

# Uninstall CRDs from a cluster
uninstall: manifests kustomize
	$(KUSTOMIZE) build config/crd | kubectl delete -f -

# Deploy controller in the configured Kubernetes cluster in ~/.kube/config
deploy: manifests kustomize
	cd config/manager && $(KUSTOMIZE) edit set image controller=$(IMG)
	$(KUSTOMIZE) build config/default | kubectl apply -f -

undeploy: manifests kustomize
	$(KUSTOMIZE) build config/default | kubectl delete -f -

# Generate manifests e.g. CRD, RBAC etc.
manifests: controller-gen
	$(CONTROLLER_GEN) $(CRD_OPTIONS) rbac:roleName=manager-role webhook paths="./..." output:crd:artifacts:config=config/crd/bases

clean-bundle:
	rm -rf bundle/manifests

# Generate bundle manifests and metadata, then validate generated files.
.PHONY: bundle
bundle: clean-bundle manifests kustomize
	operator-sdk generate kustomize manifests -q
	./scripts/create_kustomization.sh
	cd config/manager && $(KUSTOMIZE) edit set image controller=$(IMG)
	$(KUSTOMIZE) build config/manifests | operator-sdk generate bundle -q --overwrite --version $(subst v,,$(VERSION)) $(BUNDLE_METADATA_OPTS)
	operator-sdk bundle validate ./bundle

# Install CRDs into a cluster
install-k8s: manifests-k8s kustomize
	$(KUSTOMIZE) build config/crd_k8s | kubectl apply -f -

# Uninstall CRDs from a cluster
uninstall-k8s: manifests-k8s kustomize
	$(KUSTOMIZE) build config/crd_k8s | kubectl delete -f -

# Deploy controller in the configured Kubernetes cluster in ~/.kube/config
deploy-k8s: manifests-k8s kustomize
	cd config/manager && $(KUSTOMIZE) edit set image controller=$(IMG)
	$(KUSTOMIZE) build config/default_k8s | kubectl apply -f -

undeploy-k8s: manifests-k8s kustomize
	$(KUSTOMIZE) build config/default_k8s | kubectl delete -f -

# Generate manifests e.g. CRD, RBAC etc.
manifests-k8s: controller-gen
	$(CONTROLLER_GEN) $(CRD_OPTIONS) rbac:roleName=manager-role webhook paths="./..." output:crd:artifacts:config=config/crd_k8s/bases

clean-bundle-k8s:
	rm -rf bundle/manifests

# Generate bundle manifests and metadata, then validate generated files.
.PHONY: bundle-k8s
bundle-k8s: clean-bundle-k8s manifests-k8s kustomize
	operator-sdk generate kustomize manifests -q --output-dir config/manifests_k8s
	./scripts/create_kustomization.sh
	cd config/manager && $(KUSTOMIZE) edit set image controller=$(IMG)
	$(KUSTOMIZE) build config/manifests_k8s --output config/manifests_k8s/build.yaml
	yq e '.spec.installModes[0].supported = false' -i config/manifests_k8s/bases/gitlab-runner-operator.clusterserviceversion.yaml
	operator-sdk generate bundle --input-dir config/manifests_k8s -q --overwrite --version $(subst v,,$(VERSION)) $(BUNDLE_METADATA_OPTS)
	operator-sdk bundle validate ./bundle

# Run go fmt against code
fmt:
	go fmt ./...

# Run go vet against code
vet:
	go vet ./...

# Generate code
generate: controller-gen
	$(CONTROLLER_GEN) object:headerFile="hack/boilerplate.go.txt" paths="./..."

# Build the docker image
docker-build:
	docker buildx build --platform "${PLATFORM}" -t gitlab-runner-operator:${ARCH}-${VERSION} --output type=image --build-arg ARCH=${ARCH} --build-arg OS=${OS} --build-arg VERSION=${VERSION} .

docker-tag:
	docker tag gitlab-runner-operator:${ARCH}-${VERSION} ${PROJECT}/gitlab-runner-operator:${ARCH}-${VERSION}

# Push the docker image
docker-push:
	docker push ${PROJECT}/gitlab-runner-operator:${ARCH}-${VERSION}

# find or download controller-gen
# download controller-gen if necessary
controller-gen:
ifeq (, $(shell which controller-gen))
	@{ \
	set -e ;\
	CONTROLLER_GEN_TMP_DIR=$$(mktemp -d) ;\
	cd $$CONTROLLER_GEN_TMP_DIR ;\
	go mod init tmp ;\
	go get sigs.k8s.io/controller-tools/cmd/controller-gen@v0.6.1 ;\
	rm -rf $$CONTROLLER_GEN_TMP_DIR ;\
	}
CONTROLLER_GEN=$(GOBIN)/controller-gen
else
CONTROLLER_GEN=$(shell which controller-gen)
endif

kustomize:
ifeq (, $(shell which kustomize))
	@{ \
	set -e ;\
	KUSTOMIZE_GEN_TMP_DIR=$$(mktemp -d) ;\
	cd $$KUSTOMIZE_GEN_TMP_DIR ;\
	go mod init tmp ;\
	go get sigs.k8s.io/kustomize/kustomize/v3@v3.8.7 ;\
	rm -rf $$KUSTOMIZE_GEN_TMP_DIR ;\
	}
KUSTOMIZE=$(GOBIN)/kustomize
else
KUSTOMIZE=$(shell which kustomize)
endif

# Build the bundle image.
.PHONY: bundle-build
bundle-build:
	docker build -f bundle.Dockerfile -t $(BUNDLE_IMG) .

install-pip-dependencies:
	python3 -m pip install -r hack/scripts/requirements.txt
	python3 -m pip install -r scripts/ci/requirements.txt

certification-bundle: install-pip-dependencies
	$(KUSTOMIZE) build bundle | scripts/ci/parse_bundle.py

certification-bundle-build:
	docker buildx build --platform=${PLATFORM} -f certification.Dockerfile -t gitlab-runner-operator-bundle:${VERSION} .

certification-bundle-tag:
	docker tag gitlab-runner-operator-bundle:${VERSION} ${PROJECT}/gitlab-runner-operator-bundle:${VERSION}

certification-bundle-push:
	docker push ${PROJECT}/gitlab-runner-operator-bundle:${VERSION}

manifest-operator-image: install-pip-dependencies
	CERTIFIED=${CERTIFIED} PLATFORMS=${PLATFORMS} ./scripts/ci/build.py manifest operator --push ${VERSION} ${APP_VERSION}

build-and-push-operator-image: install-pip-dependencies
	CERTIFIED=${CERTIFIED} ./scripts/ci/build.py operator-image --push ${VERSION} ${APP_VERSION}

build-and-push-bundle-image: install-pip-dependencies
	CERTIFIED=${CERTIFIED} DEFAULT_CHANNEL=${BUNDLE_DEFAULT_CHANNEL} CHANNELS=${BUNDLE_CHANNELS} ./scripts/ci/build.py bundle --push ${VERSION} ${APP_VERSION}

build-and-push-catalog-source: install-pip-dependencies
	CERTIFIED=${CERTIFIED} PLATFORM=${PLATFORM} ./scripts/ci/build.py catalog --push ${VERSION} ${APP_VERSION}

build-and-push-catalog-source-manifest: install-pip-dependencies
	CERTIFIED=${CERTIFIED} PLATFORM=${PLATFORM} ./scripts/ci/build.py manifest catalog --push ${VERSION} ${APP_VERSION}

.PHONY: mocks
mocks: $(MOCKERY)
	find . -type f ! -path '*vendor/*' -name 'mock_*' -delete
	$(MOCKERY) -dir=./controllers/runner -all -inpkg

check_mocks:
	# Checking if mocks are up-to-date
	@$(MAKE) mocks
	# Checking the differences
	@git --no-pager diff --compact-summary --exit-code -- ./helpers/service/mocks \
		$(shell git ls-files | grep 'mock_' | grep -v 'vendor/') && \
		!(git ls-files -o | grep 'mock_' | grep -v 'vendor/') && \
		echo "Mocks up-to-date!"

.PHONY: community-operator
community-operator:
	mkdir -p community-operator/${VERSION}
	cd bundle && cp -r manifests metadata tests ../bundle.Dockerfile ../community-operator/${VERSION}
	yq e ".metadata.annotations.containerImage = \"${IMG}\"" -i community-operator/$(VERSION)/manifests/gitlab-runner-operator.clusterserviceversion.yaml
	if [[ -z "${PREV_VERSION}" ]]; then \
		yq e "del(.spec.replaces)" -i community-operator/$(VERSION)/manifests/gitlab-runner-operator.clusterserviceversion.yaml; \
	else \
		yq e ".spec.replaces = \"gitlab-runner-operator.v${PREV_VERSION}\"" -i community-operator/$(VERSION)/manifests/gitlab-runner-operator.clusterserviceversion.yaml; \
	fi

	./scripts/process_community_operator_dockerifle.py $(VERSION)

.PHONY: generate_changelog
generate_changelog: export CHANGELOG_RELEASE ?= dev
generate_changelog: $(GITLAB_CHANGELOG)
	# Generating new changelog entries
	@$(GITLAB_CHANGELOG) -project-id 22848448 \
		-release $(CHANGELOG_RELEASE) \
		-starting-point-matcher "v[0-9]*.[0-9]*.[0-9]*" \
		-config-file .gitlab/changelog.yml \
		-changelog-file CHANGELOG.md

$(GITLAB_CHANGELOG): OS_TYPE ?= $(shell uname -s | tr '[:upper:]' '[:lower:]')
$(GITLAB_CHANGELOG): DOWNLOAD_URL = "https://storage.googleapis.com/gitlab-runner-tools/gitlab-changelog/$(GITLAB_CHANGELOG_VERSION)/gitlab-changelog-$(OS_TYPE)-amd64"
$(GITLAB_CHANGELOG):
	# Installing $(DOWNLOAD_URL) as $(GITLAB_CHANGELOG)
	@mkdir -p $(shell dirname $(GITLAB_CHANGELOG))
	@curl -sL "$(DOWNLOAD_URL)" -o "$(GITLAB_CHANGELOG)"
	@chmod +x "$(GITLAB_CHANGELOG)"

$(MOCKERY): OS_TYPE ?= $(shell uname -s)
$(MOCKERY): DOWNLOAD_URL = "https://github.com/vektra/mockery/releases/download/v$(MOCKERY_VERSION)/mockery_$(MOCKERY_VERSION)_$(OS_TYPE)_x86_64.tar.gz"
$(MOCKERY):
	# Installing $(DOWNLOAD_URL) as $(MOCKERY)
	@mkdir -p $(shell dirname $(MOCKERY))
	@curl -sL "$(DOWNLOAD_URL)" | tar xz -O mockery > $(MOCKERY)
	@chmod +x "$(MOCKERY)"
